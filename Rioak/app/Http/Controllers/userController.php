<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class userController extends Controller
{
    public function viewregister(){
        return view('register');
    }
    public function index(){
        $data=User::all();
        return view('tableuser' ,['data'=>$data]);
    }
    public function userRegister(Request $request){
       $request->validate([
           'email'=>'required|unique:users,email|min:16|max:16',
           'name'=>'required'
       ],
       [
        'email.unique'=>'Nik Sudah Terdaftar',
        'name.required'=>'Nama Tidak Boleh Kosong'
       ]
       );
        
        $data = [
            'name'=>$request->name,
            'email'=> $request->email,
            'password' => bcrypt($request->email)
        ];

        User::create($data);
        return redirect('/')->with('message','penyimpanan berhasil');
        
    }
}
