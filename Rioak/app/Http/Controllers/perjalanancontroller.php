<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\perjalanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Cloner\Data;
use Illuminate\Support\Facades\Auth;

class perjalanancontroller extends Controller
{
    public function index(){
        $data = Auth::user()->perjalanan()->get();
        return view('table' ,['data'=>$data]);
    }

    public function inputperjalanan(){
        return view('form');
    }

    public function simpanperjalan(Request $request){
        $id_user = Auth::user()->id;
        $data=[
            'id_user'=> $id_user,
            'tanggal'=>$request->tanggal,
            'jam'=>$request->jam,
            'lokasi'=>$request->lokasi,
            'suhu'=>$request->suhu
        ];
        // dd($data);
        perjalanan::create($data);
        return redirect('/catatanPerjalanan')->with('message','penyimpanan berhasil');
    }

    public function cariPerjalanan(Request $request)
    {
        if (!empty($request->input('lokasi')) && empty($request->input('suhu')) && empty($request->input('tanggal')) && empty($request->input('jam'))) {
            $cari = $request->lokasi;
            $data = User::join('perjalanans', 'perjalanans.id_user', '=', 'users.id')
                ->where(function ($query) use ($cari) {
                    $query->where('users.name', auth()->user()->name)
                        ->where('perjalanans.lokasi', 'LIKE', '%' . $cari . '%');
                })->get(['users.name', 'perjalanans.*']);
            if ($data) {
                return view('table', ['data' => $data])->with('alert', 'data ditemukan');
            } else {
                abort(404);
            }
        } elseif (empty($request->input('lokasi')) && !empty($request->input('suhu')) && empty($request->input('tanggal')) && empty($request->input('jam'))) {
            $cari = $request->suhu;
            $data = User::join('perjalanans', 'perjalanans.id_user', '=', 'users.id')
                ->where(function ($query) use ($cari) {
                    $query->where('users.name', auth()->user()->name)
                        ->where('perjalanans.suhu', 'LIKE', '%' . $cari . '%');
                })->get(['users.name', 'perjalanans.*']);
            if ($data) {
                return view('table', ['data' => $data])->with('alert', 'data ditemukan');
            } else {
                abort(404);
            }
        } elseif (empty($request->input('lokasi')) && empty($request->input('suhu')) && !empty($request->input('tanggal')) && empty($request->input('jam'))) {
            $cari = $request->tanggal;
            $data = User::join('perjalanans', 'perjalanans.id_user', '=', 'users.id')
                ->where(function ($query) use ($cari) {
                    $query->where('users.name', auth()->user()->name)
                        ->where('perjalanans.tanggal', 'LIKE', '%' . $cari . '%');
                })->get(['users.name', 'perjalanans.*']);
            if ($data) {
                return view('table', ['data' => $data])->with('alert', 'data ditemukan');
            } else {
                abort(404);
            }
        } elseif (empty($request->input('lokasi')) && empty($request->input('suhu')) && empty($request->input('tanggal')) && !empty($request->input('jam'))) {
            $cari = $request->jam;
            $data = User::join('perjalanans', 'perjalanans.id_user', '=', 'users.id')
                ->where(function ($query) use ($cari) {
                    $query->where('users.name', auth()->user()->name)
                        ->where('perjalanans.jam', 'LIKE', '%' . $cari . '%');
                })->get(['users.name', 'perjalanans.*']);
            if ($data) {
                return view('table', ['data' => $data])->with('alert', 'data ditemukan');
            } else {
                abort(404);
            }
        } else {
            $data = DB::table('perjalanans')
            ->join('users', 'users.id', '=', 'perjalanans.id_user')
            ->select('users.name', 'perjalanans.tanggal', 'perjalanans.jam', 'perjalanans.suhu', 'perjalanans.lokasi')
            ->where('users.id', '=', auth()->user()->id)
            ->get();

            $mode = $request -> query('mode');
            $field  = $request -> query('field');

            if($mode && $field){
                $data = DB::table('perjalanans')
                ->orderBy($field , $mode)
                ->get();
            }
            return view('table', ['data' => $data]);
        }
    }
}
