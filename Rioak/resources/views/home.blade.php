@extends('layouts.master')
@section('content')
    <section class="section">
        @if(session()->has('message'))
        <script>
          alert("Selamat Datang Di Peduli Diri")
        </script>
      @endif
        <div class="section-header">
            <h1>Home</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Home</a></div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Home</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Peduli Diri</h4>
                        </div>
                        <div class="card-body">
                            Selamat Datang     
                            @if (!empty(auth()->user()->name))
                                {{auth()->user()->name}}
                            @else
                                user
                            @endif    Di Aplikasi Peduli Diri
                        </div>
                  <div class="card-footer">
                    
                  </div>
                </div>
            </div>
        </div>
    </section>              

@endsection