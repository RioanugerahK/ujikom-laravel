@extends('layouts.master')
@section('content')
<section class="section">
  @if(session()->has('message'))
    <script>
      alert("Berhasil Menambahkan Data")
    </script>
  @endif
    <div class="section-header">
      <h1>Tabel Catatan Perjalanan</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Home</a></div>
        <div class="breadcrumb-item">Catatan Perjalanan</div>
      </div>
    </div>  

    <div class="section-body">
      <h2 class="section-title">Catatan Perjalanan</h2>
      <p class="section-lead">
        CEK CATATAN PERJALANAN KALIAN DI BAWAH INI !
      </p>
      <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>Catatan Perjalan</h4>
            </div>
            <div class="card-body">
              <form action="/cari" method="GET">
                @csrf
                <div class="col-auto">
                  <select onchange="yesnoCheck(this);" class="form-control form-select" type="search">
                    <option value="lokasi">Lokasi</option>
                    <option value="tanggal">tanggal</option>
                    <option value="jam">jam</option>
                    <option value="suhu">suhu</option>
                  </select>
                </div>
                <div class="col-auto mt-1" id="lokasi">
                  <input type="search" name="value" class="form-control" placeholder="Cari Lokasi" aria-label="search">
                  <button class="btn btn-success mt-2" type="submit" value="submit" style="float: right ;">Cari Data Perjalanan</button>
                </div> 
                <div class="form-group" id="tanggal">
                  <input type="date" name="tanggal" style="display: none;"  class="form-control" placeholder="Cari Tanggal" aria-label="search">
                  <button class="btn btn-success" style="display: none; float: right ;" value="submit" class="btn btn-primary" type="submit">Cari Data Perjalanan</button>
                </div> 
               
                <div class="form-group" id="jam">
                  <input type="time" name="jam"  style="display: none;" class="form-control" placeholder="Cari Jam" aria-label="search">
                  <button class="btn btn-success" style="display: none; float: right ;" value="submit" class="btn btn-primary" type="submit">Cari Data Perjalanan</button>
                </div> 
                <div class="form-group" id="suhu">
                  <input type="number" name="suhu" style="display: none;" class="form-control" placeholder="Cari Suhu" aria-label="search">
                  <button class="btn btn-success" value="Submit" type="submit"  style="float: right ; display: none; ">Cari Data Perjalanan</i></button>
                </div>  
              </form>
            </div>
              <table class="table">
                @php
                $no = 1    
                @endphp
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">
                      tanggal
                      <a href="/short?field=tanggal&mode=asc"><i class="fas fa-arrow-up"></i></i></a>
                      <a href="/short?field=tanggal&mode=desc"><i class="fas fa-arrow-down"></i></a>
                    </th>
                    <th scope="col">
                      lokasi
                      <a href="/short?field=lokasi&mode=asc"><i class="fas fa-arrow-up"></i></a>
                      <a href="/short?field=tanggal&mode=desc"><i class="fas fa-arrow-down"></i></a>
                    </th>
                    <th scope="col">
                      jam
                      <a href="/short?field=jam&mode=asc"><i class="fas fa-arrow-up"></i></a>
                      <a href="/short?field=tanggal&mode=desc"><i class="fas fa-arrow-down"></i></a>
                    </th>
                    <th scope="col">
                      suhu
                      <a href="/short?field=suhu&mode=asc"><i class="fas fa-arrow-up"></i></a>
                      <a href="/short?field=tanggal&mode=desc"><i class="fas fa-arrow-down"></i></a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $rio_peduli_diri)
                  <tr>
                    <th scope="row">{{$no++}}</th>
                    <td>{{$rio_peduli_diri->tanggal}}</td>
                    <td>{{$rio_peduli_diri->lokasi}}</td>
                    <td>{{$rio_peduli_diri->jam}}</td>
                    <td>{{$rio_peduli_diri->suhu}}</td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
        </div>
      </div>
    </div>
  </section>

  <script>
    function yesnoCheck(that){
        if (that.value == 'tanggal'){
          document.getElementById('tanggal').style.display = 'block';
        }else if(that.value == 'jam'){
          document.getElementById('jam').style.display = 'block';
          document.getElementById('tanggal').style.display = 'none';
        }else if(that.value == 'suhu'){
          document.getElementById('suhu').style.display = 'block';
          document.getElementById('jam').style.display = 'none';
        }else{
          document.getElementById('lokasi').style.display = 'block';
           document.getElementById('suhu').style.display = 'none';
        }
    }
  </script>

  @endsection