@extends('layouts.master')

@section('content')
    



<section class="section">
    <div class="section-header">
      <h1>Profile</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item">Profile</div>
      </div>
    </div>
    <div class="section-body">
      <h2 class="section-title">Hi, Rio!</h2>
      <p class="section-lead">
        Change information about yourself on this page.
      </p>

      <div class="row mt-sm-4 justify-content-center">
        <div class="col-12 col-md-12 col-lg-5">
          <div class="card profile-widget">
            <div class="profile-widget-header">
              <img alt="image" src="../assets/img/avatar/avatar-1.png" class="rounded-circle profile-widget-picture">
              <div class="profile-widget-items">
                <div class="profile-widget-item">
                  <div class="profile-widget-item-label">Posts</div>
                  <div class="profile-widget-item-value">2</div>
                </div>
                <div class="profile-widget-item">
                  <div class="profile-widget-item-label">Followers</div>
                  <div class="profile-widget-item-value">731</div>
                </div>
                <div class="profile-widget-item">
                  <div class="profile-widget-item-label">Following</div>
                  <div class="profile-widget-item-value">767</div>
                </div>
              </div>
            </div>
            <div class="profile-widget-description">
              <div class="profile-widget-name">@if (!empty(auth()->user()->name))
                {{auth()->user()->name}}
            @else
            user
            @endif     <div class="text-muted d-inline font-weight-normal"><div class="slash"></div> Web Developer</div></div>
            @if (!empty(auth()->user()->name))
            {{auth()->user()->name}}
        @else
        user
        @endif     is a superhero name in <b>Indonesia</b>, especially in my family. He is not a fictional character but an original hero in my family, a hero for his children and for his wife. So, I use the name as a user in this template. Not a tribute, I'm just bored with <b>'John Doe'</b>.
            </div>
            <div class="card-footer text-center">
              <div class="font-weight-bold mb-2">Follow <div class="d-sm-none d-lg-inline-block">
                @if (!empty(auth()->user()->name))
                     {{auth()->user()->name}}
                 @else
                 user
                 @endif    
               </div> On</div>
              <a class="nav-link" style="font-size: 20px" href="https://instagram.com/rioanugerah.k"><i class="fab fa-instagram"></i><span>rio anugerah k</span></a>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>

  @endsection