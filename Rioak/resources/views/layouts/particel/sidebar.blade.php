<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="/home">SMKN 2 Bandung </a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="/home"><img src="{{asset('assets/img/smkn2.png')}}" alt="" style="width: 50px"></a>
      </div>
      <ul class="sidebar-menu">
        <li class="active"><a class="nav-link" href="/catatanPerjalanan"><i class="fas fa-home" style="font-size: 25px"></i> <span>Home</span></a></li>
        <li class="active"><a class="nav-link" href="/catatanPerjalanan"><i class="fas fa-book" style="font-size: 25px"></i> <span>Catatan Perjalanan</span></a></li>
        <li class="active"><a class="nav-link" href="/form"><i class="fas fa-pen-square" style="font-size: 25px"></i> <span>Input Perjalanan</span></a></li>
        <li class="active"><a class="nav-link" href="/tableuser"><i class="fas fa-book" style="font-size: 25px"></i> <span>Data Login</span></a></li>
    </aside>
  </div>