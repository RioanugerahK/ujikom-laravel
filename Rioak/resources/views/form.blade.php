@extends('layouts.master')
@section('content')
<section class="section">
    <div class="section-header">
      <h1>Form</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Bootstrap Components</a></div>
        <div class="breadcrumb-item">Form</div>
      </div>
    </div>

    <div class="section-body">
      <h2 class="section-title">Masukan Catatan Perjalanan</h2>

      <div class="row" >
        <div class="col-12 col-md-6 col-lg-10">
          <div class="card">
            <div class="card-header">
              <h4>Peduli Diri</h4>
            </div>
            <div class="card-body">
              <div class="alert alert-info">
                <b>Note!</b> Masukan Data Yang Valid
              </div>
              <form action="/formInput" method="POST">
              {{ csrf_field() }}
              
                    {{-- Tanggal --}}
              <div class="form-group"> 
                <label>Tanggal</label>
                <input type="date" class="form-control" name="tanggal" required>
              </div>
                    {{-- Jam --}}
              <div class="form-group">
                <label>Jam</label>
                <input type="time" class="form-control" name="jam" required>
              </div>
              {{-- Lokasi --}}
              <div class="form-group">
                <label>Lokasi Yang Dikunjungi</label>
                <input type="text" class="form-control" name="lokasi" required>
              </div>
              {{-- Suhu Tubuh --}}
              <div class="form-group" required>
                <label>Suhu Tubuh</label>
                <input type="number" class="form-control" name="suhu" required>
              </div>
         
              {{-- Submit --}}
            <div class="card-footer text-right">
              <button class="btn btn-primary mr-1" type="submit">Submit</button>
              <button class="btn btn-secondary" type="reset">Reset</button>
            </div>
              </form>
          </div>
        <div class="col-12 col-md-6 col-lg-6">
        </div>
      </div>
    </div>
  </section>
@endsection