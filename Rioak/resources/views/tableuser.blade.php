@extends('layouts.master')
@section('content')
<section class="section">
    <div class="section-header">
      <h1>Data Login User</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Bootstrap Components</a></div>
        <div class="breadcrumb-item">Data Login</div>
      </div>
    </div>

    <div class="section-body">
      <h2 class="section-title">Tables Data Login</h2>
      <p class="section-lead">
        CEK DATA LOGIN USER DI BAWAH INI
      </p>

      <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>USERS</h4>
            </div>
            <div class="card-body">
              <table class="table">
                @php
                $no = 1    
                @endphp
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">nama</th>
                    <th scope="col">nik</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $rio_peduli_diri)
                  <tr>
                    <td>{{$rio_peduli_diri->id}}</td>
                    <td>{{$rio_peduli_diri->name}}</td>
                    <td>{{$rio_peduli_diri->email}}</td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
        </div>
      </div>
    </div>
  </section>

  @endsection