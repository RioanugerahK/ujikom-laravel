<?php

use App\Http\Controllers\perjalanancontroller;
use App\Http\Controllers\usercontroller;
use App\Http\Controllers\loginController;
use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\shortercontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// login
Route::get('/',[loginController::class,'halamanLogin'])->name('login');
Route::post('/postlogin',[loginController::class,'login']);
Route::get('/home', function(){
    return view('home');
});
// logout
Route::get('logout',[loginController::class,'Logout']);
Route::get('/catatanPerjalanan',[perjalanancontroller::class,'index']);
Route::get('/tableuser',[userController::class,'index']);
Route::get('/form',[perjalanancontroller::class,'inputperjalanan']);
Route::post('/formInput',[perjalanancontroller::class,'simpanperjalan']);
Route::get('/cari',[perjalanancontroller::class,'cariPerjalanan']);
Route::get('/short', [shortercontroller::class,'shorterData']);
Route::get('/profile', function(){
    return view('profile');
});
Route::get('/register',[userController::class,'viewregister']);
Route::post('/registerUser',[usercontroller::class,'userRegister']);